/*
�������������� ������� ������.
����������:
	������ � �������, ���, ��������, ���� ��������, ������ ����������, � ������� ����� �����, ����, ����������� � ������ ����������; class Actor,class Ticket
	��������� � ����� �����, ��������, �����������, ������ ����������� ��� � ������������; class Speclacle,class Ticket
	����� � �������� ���������, ����, �����; ������ � ����, ���������� ������� ������� ���� (������, ����, ������ � ��.). class Schedule,class Ticket
��������:
	��������� ������ ������� � ������ ����������; EditActor() EditSpectacle()
	����������� � ��������� �����;EditSchedule() AddSchedule()
	������� ������� �� ���������; kassirMenu()
	������� (������� ������� � ��������� �����������, ���������� �� ���������� � ��.). SpectaclePrint(), ActorPrint(), SchedulePrint();
������������� ��������� ������ �������������: 
	�������������, adminMenu()
	������, kassirMenu()
	�������. viewerMenu()
					����� ����������
1. ������ ������� � �������� ������. ��� ��������� ������������ ������������ ������. load() save()
���� � ����� ������ ������������ ������ ����� ���������������� ��������� ��������������� �������. menus
2. ������������� �������� ������������ ��������� ������. readDate() readTime()
3. ������������� �����  ActorFind()  SpectacleFind()
� ���������� ������ �� ��������� ����������. SortActorByName(), SortActorByBirthday(),SortSpectacleByTitle(),SortSpectacleByAuthor()
 */
// ����� � ������ ������ ���� ��� � � ���� � �� �� ����� ����� ���� ������ ���� ���������.
/*
1. ������ ����� � ��������� �������.
���� ���������� ������ schedule, ������� �������� ������� ���� Schedule, ������� �������� ������ ���� Ticket. - ��������� ������. ��. void kassirMenu() ������ ��������� ����� ���������� ������.
����� ������� ��� ����� ���������� ������� ����� ������� ��������� ������? 
���� ���������� ������ ����� � ��������� (���� �� ����� Schedule), �� ���������, ��� � ������� ��������� ���� ������, �� �� ��� ��������� ����� ����������������� � �� � ���� ����� ���� ������ � ���������� ������ �������
���� ���������� ���� � ����� � ����� ��� vector<DateTime>, �� � ������� ������ ����� ������ � ������������ ��������� ������ - ���� ����� = ���� ����� � ����
2. ����������� ����������� ������� ������. 
3. ���� ����������� ����� ������� ���� ����� ����. ��� ������ �������� ���� ������� ������, � ����� ������ ��� �� ���������.
4. ����� ������ ������ ��� ������ ������� ���������. � ���������, ����� ������� ���� users.txt � ��������� ��� �� ������� %username% %password% admin, ����� ��������� ������ ��� ��������������. 
�����, �������, �������� � ��� ����������� ������, �� ��� ����� ������ �� ���������� ����� � �������.
 */
#include <iostream> // �������
#include <clocale>// ������� ����
#include <fstream> // �����

#include <vector> // ������ ������
#include <string> // ������
#include <sstream> // ������ �� ����������� ����� � �������, ������ ��� ����� � ������
#include <algorithm> // ����������

using namespace std;

class Spectacle; // ���������
class Ticket; // ������
class Actor; // �����
class Schedule; // ����������
class Role; // ���� ������ � ����������
/* ���������� ���������� */
vector<Schedule> schedule; // �����
vector<Actor> actors; // �����
vector<Spectacle> spectacles; // ���������
vector<Role> roles; // ���� ������ � ����������
/* ������ � ������� */
void load(); // ������� � �����
void save(); // ��������� � ����
/* ���� � ���������� (��������) */
bool welcome(); // �� ����������������?
bool auth(); // �������� ������. ����������, ��������� ������ ��� �����
	void viewerMenu(); // ���� ����������
		void SpectaclePrint(); // ����� ����������
		void ActorPrint(); // ����� ������
		void SchedulePrint(); // ����� �����
		void ActorFind(); // ����� ����� �� �����
		void SpectacleFind(); // ����� ���������� �� ��������
		void SortActorByName(); // ���������� ������ �� �����
		void SortActorByBirthday(); // ���������� ������ �� ��� ���� ��������
		void SortSpectacleByTitle(); // ���������� ���������� �� ��������
		void SortSpectacleByAuthor(); // ���������� ���������� �� �������
	void kassirMenu(); // ������� �������
	void adminMenu(); // ���� ��������������
		void EditEntityMenu(); // ���� ��������������
			void EditActor(); // �������������� �����
			void EditSpectacle(); // �������������� ����������
			void EditSchedule(); // �������������� �����
		void AddEntityMenu(); // ���� ����������
			void AddUser(); // ���������� ������������
			void AddActor(); // ���������� �����
			void AddSpectacle(); // ���������� ���������
			void AddSchedule(); // ���������� �����
		void DeleteEntityMenu(); // ���� ��������
			void DelActor(); // �������� �����
			void DelSpectacle(); // �������� ���������
			void DelSchedule(); // �������� �����
			
int main() { // ������� �������
	setlocale(LC_ALL, "Rus"); // ����� ��� ������� ����
	load(); // �������� � ����� ������
	if(welcome()) viewerMenu(); // ���������, ���� �� ������������ � ������ ������������������
		else // ���� �� ���������������
			if(auth()) adminMenu(); // �����������. ���� �� ������� - ��������� �����������, ���� ������� - � ����������� �� ������� ������������ ���� ����� �����
			else kassirMenu(); // ���� ����� �������
	save(); // ���������� ������ � ����
	return 0; // ����� ���������
}

/* ���������� */
class Spectacle // ���������
{
private:
	string author; // �����
	string title; // ��������
	string director; // ����������� 
	
public:
	Spectacle( string author,  string title,  string director)
		: author(author),
		  title(title),
		  director(director) {} // ����������� 
	// �������, ������� ���������� �������� ��������� �����
	string getAuthor()  { return author; }
	string getTitle()  { return title; }
	string getDirector()  { return director; }
	// �������, ������� ������������� �������� ��������� �����
	void setAuthor( string author) { this->author = author; }	
	void setIitle( string title) { this->title = title; }	
	void setDirector( string director) { this->director = director; }

	string ToString() // ��� ������ �� �������
	{
		stringstream stream; // � ���� ���������� ����� ������� ������
		stream << "C�������� :"   << this->getTitle()    << endl
			   << "����� �����: " << this->getAuthor()   << endl
			   << "�����������: " << this->getDirector() << endl; // ��������� � ���������� ��� ������ ����
		string result = stream.str(); // ������� ������ �� ����������
		return  result;
	}
	static Spectacle find(string title)  // ���������� ����� �� ��������
	{
		for(Spectacle i : spectacles) 
		{ 
			if(i.getTitle().compare(title) == 0) return i; // ���� �������� ������������, �� ���������� ��������� �������
		}
	}
};
class Actor // �����
{
private:
	string FIO; // ���
	string birthday; // ���� �������
public:
	Actor( string fio,  string birthday)
		: FIO(fio),
		birthday(birthday)
	{} // �����������
	   // �������, ������� ���������� �������� ��������� �����
	string getFIO()  { return FIO; }	
	string getBirthday()  { return birthday; }
	// �������, ������� ������������� �������� ��������� �����
	void setFIO(string fio) { FIO = fio; }
	void setBirthday( string birthday) { this->birthday = birthday; }
	

	string ToString()// ��� ������ �� �������
	{
		stringstream ss;// � ���� ���������� ����� ������� ������
		ss << "���:" << this->getFIO() << "\n"
			<< "���� ��������: " << this->getBirthday() << "\n";// ��������� � ���������� ��� ������ ����
		string result = ss.str();// ������� ������ �� ����������
		return result;
	}
	static Actor find(string FIO) {// ���������� ����� �� �����
		for(Actor i : actors)
		{
			if(i.getFIO().compare(FIO) == 0) return i; // ���� ��� �������������, �� ���������� ��������� �������
		}
	}
};
class Role // ����
{
public:
	Spectacle spectacle;
	Actor actor;
	string role;
	Role( Spectacle spectacle,  Actor actor, string role)
		: spectacle(spectacle),
		  actor(actor),role(role) {} // �����������
	// �������, ������� ���������� �������� ��������� �����
	Spectacle getSpectacle() { return spectacle; }
	string getRole() { return role; }
	Actor getActor() { return actor; }
	// �������, ������� ������������� �������� ��������� �����
	void set_spectacle(Spectacle spectacle) { this->spectacle = spectacle; }
	void set_actor(Actor actor) { this->actor = actor; }
	void set_role(string role) { this->role = role; }
};
class Ticket // ������
{
private:
	int type; // ��� 0 - ������, 1 - ���� �����, 2 - ���� ������, 3 - ������
	int row; // ���
	int place; // ����� 

public:
	Ticket( int type,  int row,  int place)
		: type(type),
		  row(row),
		  place(place) {} // �����������
	// �������, ������� ���������� �������� ��������� �����
	int getType() { return type; }
	int getRow() { return row; }
	int getPlace() { return place; }
	// �������, ������� ������������� �������� ��������� �����
	void setType( int type) { this->type = type; }
	void setRow( int row) { this->row = row; }
	void setPlace( int place) { this->place = place; }
	string ToString() { // ��� ������ �� �������
		stringstream ss; // � ���� ���������� ����� ������� ������
		switch (this->type)
		{
		case 0: ss << "������";break;
		case 1: ss << "���� ������";break;
		case 2: ss << "���� �����";break;
		case 3: ss << "������";break;
		}
		cout << "��� " << this->row << "����� " << this->place << endl;// ��������� � ���������� ��� ������ ����
		string result = ss.str();// ������� ������ �� ����������
		return result;
	}
};
class Schedule // �����
{
private:
	Spectacle spectacle; // ���������
	vector<Ticket> tickets; // ������ �� ����
	string date;// ���� ���������
	string time; // ����� ���������

public:
	Schedule( Spectacle spectacle,  vector<Ticket> tickets,  string date,  string time)
		: spectacle(spectacle),
		tickets(tickets),
		date(date),
		time(time)
	{} // ����������� ��� �������

	Schedule( Spectacle spectacle,  string date,  string time)
		: spectacle(spectacle),
		date(date),
		time(time)
	{} // ����������� � ��������

	string ToString() // ��� ������ �� �������
	{
		stringstream ss;// � ���� ���������� ����� ������� ������
		ss << "C��������:" << this->spectacle.ToString() << "\n"
		   <<"����:" << this->date << "�����:" << this->time << endl
			<< "��������� ������: " << "\n";// ��������� � ���������� ��� ������ ����
		for(Ticket i : this->tickets)
		{
			ss << i.ToString();
		}
		string result = ss.str();// ������� ������ �� ����������
		return result;
	}
	// �������, ������� ���������� �������� ��������� �����
	Spectacle getSpectacle()  { return spectacle; }
	vector<Ticket> getTicket() { return tickets; }
	string getDate() { return date; }
	string getTime() { return time; }
	// �������, ������� ������������� �������� ��������� �����
	void setSpectacle( Spectacle spectacle) { this->spectacle = spectacle; }
	void setTicket( vector<Ticket> tickets) { this->tickets = tickets; }
	void setDate( string date) { this->date = date; }
	void setTime( string time) { this->time = time; }
};
/* ����� ������ */
string readDate() { // �������� ���� � ������� � ��������� ����
	int tempInt = 0; // ��������� ����������, ����� ��������� ���� ������� �����
	stringstream ss; // � ���� ���������� ����� ������� ������ c �����
	while(tempInt < 1 || tempInt >31) // ���� ���� ������� �� ������ �������
	{
		cout << "������� ���� (1-31):";
		cin >> tempInt;
	} // ������ � ������� ����
	ss << tempInt << "."; // ��������� ��� � ������
	tempInt = 0; // �������� ��������� ����������
	while(tempInt < 1 || tempInt >12) // ���� ����� ������� �� ������ �������
	{
		cout << "������� �����(1-12):";
		cin >> tempInt;
	} // ������ � ������� �����
	ss << tempInt << "."; // ��������� ��� � ������
	tempInt = 0; // �������� ��������� ����������
	while(tempInt < 1900 || tempInt >2017) // ���� ��� ������� �� ������ �������
	{
		cout << "������� ���(1900-2017):";
		cin >> tempInt;
	} // ������ � ������� ���
	ss << tempInt << "."; // ��������� ��� � ������
	string date = ss.str(); // ������� ������ �� ����������
	return date;
}
string readTime() { // �������� ����� � ������� � ��������� ����
	stringstream ss;  // � ���� ���������� ����� ������� ������ c ��������
	int tempInt = -1; // ��������� ����������, ����� ��������� ���� ������� �����
	while(tempInt < 0 || tempInt >23) // ���� ��� ������� �� ������ �������
	{
		cout << "������� � ����� ���� ������(0-23):";
		cin >> tempInt;
	} // ������ ��� � �������
	ss << tempInt << ":"; // ��������� ��� � ������
	tempInt = -1; // �������� ��������� ����������
	while(tempInt < 0 || tempInt >59) // ���� ������ ������� �� ������ �������
	{
		cout << "������� ������ ������(0-59):";
		cin >> tempInt;
	} // ������ � ������� ������
	ss << tempInt; // ��������� � � ������
	string time = ss.str(); // ������� ������ �� ����������
	return time;
}
void load() { // �������� �� �����
	// ��������� ������ ����� �� ������
	ifstream scheduleIN("schedule.bin", ios::binary);
	ifstream actorsIN("actors.bin", ios::binary);
	ifstream ticketIN("ticket.bin", ios::binary);
	ifstream roleIN("map.bin", ios::binary);
	// ������
	scheduleIN.read((char*) &schedule, sizeof(Schedule));
	actorsIN.read((char*) &actors, sizeof(Actor));
	ticketIN.read((char*) &spectacles, sizeof(Spectacle));
	roleIN.read((char*) &roles, sizeof(Role));
	// ��������� ��� �����
	scheduleIN.close();
	actorsIN.close();
	ticketIN.close();
	roleIN.close();
}
void save() // ���������� � ����
{
	// ��������� ������ ����� �� ������
	ofstream scheduleOUT("schedule.bin", ios::binary);
	ofstream actorsOUT("actors.bin", ios::binary);
	ofstream ticketOUT("ticket.bin", ios::binary);
	ofstream roleOUT("role.bin", ios::binary);
	//�����
	for(Schedule i : schedule)
		scheduleOUT.write((char*) &i, sizeof(Schedule));
	for(Actor i : actors)
		actorsOUT.write((char*) &i, sizeof(Actor));
	for(Spectacle i : spectacles)
		ticketOUT.write((char*) &i, sizeof(Spectacle));
	for(Role i : roles)
		roleOUT.write((char*) &i, sizeof(Role));
	// ��������� ��� �����
	scheduleOUT.close();
	actorsOUT.close();
	ticketOUT.close();
	roleOUT.close();
}
/* ����������� ����*/
bool welcome() { // ���� ������, ��������������� �� ������������
	char answer;
	cout << "����� ����������. �� ������������������ ������������?(y/n)";
	cin >> answer;
	return 'n' == answer; // ���� �� ���������������, ��� � ����������.
}
bool auth() { // �������� ������ � �����
	ifstream usersIN("users.txt"); // ��������� ���� � ��������
	string rUser, rPassword; // ��������� ���������� �� ������ � �������
	//������ � ������� 
	cout << "������� �����:"; cin >> rUser;
	cout << "������� ������:"; cin >> rPassword;
	string user, password, status; // ��������� ���������� �� ������ � �����
	//������ � �����
	while(usersIN >> user >> password >> status) // ���� � ����� �������� ������
	{
		if(0 == rUser.compare(user) && 0 == rPassword.compare(password)) // ���� � ����� ������� ������ � �����, ������� ��������� � ��������
			return status.compare("admin") == 0; // ���������� ������ ����������������� ������������
	}
	cout << "����� �� ������."; // ����� ������� ������
	main(); // � ������������ � ����
	return 0;
}

typedef void(*menus)(); // ������ ��� ��� �������� ����
menus viewerMenuSelector[9] =
											{
												ActorPrint,
												SpectaclePrint,
												SchedulePrint,
												ActorFind,
												SpectacleFind,
												SortActorByName,
												SortActorByBirthday,
												SortSpectacleByTitle,
												SortSpectacleByAuthor
											}; // ������� ������ ���� ��� ��������������������� ������������
/* ���� �� ������������������� */
void viewerMenu() // ���� ��������������������� ������������
{
	cout << "������� �������, ����� ����������!" << endl;
	cout << "�������� ����� ����: " << endl
		<< "1. �������� ������ ������ ������ ������" << endl
		<< "2. �������� ������ ���������� ������ ������" << endl
		<< "3. �������� ����� ������ ������" << endl
		<< "4. ����� �� ������" << endl
		<< "5. C��������� ������ �� ������ ����� �����" << endl		
		<< "6. ���������� ������ �� ��� ��������" << endl		
		<< "7. ���������� ��������� �� ������ ����� ��������" << endl		
		<< "8. ���������� ��������� �� ������ ����� ����� ������" << endl		
		<< "9. �����" << endl; // ������� ����
	
	int choice;
	cin >> choice; // ������ ����� ������������
	if(choice == 9) main(); // ���� ��� "�����", ��� � ���������
	else
	{
		(*viewerMenuSelector[choice - 1])(); // ����� ����������� ������ ����
		system("cls"); // ������ ������
		viewerMenu(); // ����������� ����� ���� �� ����
	}
}
void ActorPrint() // ����� ����� � �������
{
	cout << "������ ����� ������:" << endl;
	for(Actor i : actors) // ����������� �� ���� ������
	{
		cout << i.ToString() << endl; // ������� ������� ����� � ������� ����� �������, ��������� ����
		for (Role j : roles) // ����������� �� ���� �����
		{
			if(j.actor.getFIO().compare(i.getFIO()) == 0) // ���� ������� ���� ����������� ������� ������
				cout << "���������: " << j.getSpectacle().ToString() 
					 << " ����: "     << j.getRole(); // ������� ���
		}
	}
}
void SpectaclePrint() //����� ��������� � �������
{
	cout << "����� ����� ����������:" << endl;
	for(Spectacle i : spectacles) // ����������� �� ���� ����������
	{
		cout << i.ToString() << endl; // ������� ������ ��������� � ������� ����� �������, ��������� ����
		for(Role j : roles)
		{
			if(0 == j.getSpectacle().getTitle().compare(i.getTitle())) // ���� ������� ���� �������� ������� ���������
				cout << " ����: " << j.getRole()
				     << " ����: " << j.getActor().ToString() << endl; // ������� ��� ����
		}
	}
}
void SchedulePrint() // ����� �����
{
	cout << "�����:" << endl;
	for(Schedule i : schedule) cout << i.ToString() << endl; // ����������� �� ���� ������ � ������� �� ����� �������, ��������� ����
}
void ActorFind() { // ����� ����� �� ����� �����
	cout << "������� ����� ����� �����:";
	string find;
	getline(cin, find); // ������ � ������� �������
	for(Actor i : actors) // ����������� �� ���� ������
		if(i.getFIO().find(find) != string::npos) cout << i.ToString() << endl; // ���� ������� ��� ����� �������� �������, ������� ����� ����� ����� �������, ��������� ����
}
void SpectacleFind() { // ����� ��������� �� ����� ��������
	cout << "������� ����� ������� ���������:";
	string find;
	getline(cin, find);// ������ � ������� �������
	for(Spectacle i : spectacles) // ����������� �� ���� ����������
		if(i.getTitle().find(find) != string::npos) cout << i.ToString() << endl;// ���� ������� �������� ��������� �������� �������, ������� ���� ��������� ����� �������, ��������� ����
}
bool greaterActorByName(Actor l, Actor r) { return l.getFIO().at(0) > r.getFIO().at(0); } // ������� ��� �����������, ����� ��� ����� ����� ���� � ����������. ������������ � sort()
bool greaterActorByBirthday(Actor l, Actor r) { return stoi(l.getBirthday()) > stoi(r.getBirthday()); } // ������� ��� �����������, ����� ���� �������� ����� ���� � ����������. ����������� ������ ����. ������������ � sort()
bool greaterSpectacleByTitle(Spectacle l, Spectacle r) { return l.getTitle().at(0) > r.getTitle().at(0); }; //  ������� ��� �����������, ����� �������� ��������� ����� ���� � ����������. ����������� ������ ������� �����.  ������������ � sort()
bool greaterSpectacleByAuthor(Spectacle l, Spectacle r) { return l.getAuthor().at(0) > r.getAuthor().at(0); }; // ������� ��� �����������, ����� ��� ������ ��������� ����� ���� � ����������. ����������� ������ ������� �����.  ������������ � sort()
void SortActorByName() { // ���������� ������ �� �����
	vector<Actor> sortactors(actors); // ������ ����� ������������� �������
	sort(sortactors.begin(),sortactors.end(),greaterActorByName); // ���������� �� ��������� �������
	for(Actor i : sortactors) { cout << i.ToString() <<endl; } // ����� ���������������� �������
}
void SortActorByBirthday() { // ���������� ������ �� ��� ��������
	vector<Actor> sortactors(actors);// ������ ����� ������������� �������
	sort(sortactors.begin(), sortactors.end(), greaterActorByBirthday); // ���������� �� ��������� �������
	for(Actor i : sortactors) { cout << i.ToString() << endl; }// ����� ���������������� �������
}
void SortSpectacleByTitle() { // ���������� ���������� �� ��������
	vector<Spectacle> sortSpectacles(spectacles);// ������ ����� ������������� �������
	sort(sortSpectacles.begin(), sortSpectacles.end(), greaterSpectacleByTitle); // ���������� �� ��������� �������
	for(Spectacle i : sortSpectacles) { cout << i.ToString() << endl; }// ����� ���������������� �������
}
void SortSpectacleByAuthor() { // ���������� ���������� �� ������
	vector<Spectacle> sortSpectacles(spectacles);// ������ ����� ������������� �������
	sort(sortSpectacles.begin(), sortSpectacles.end(), greaterSpectacleByAuthor); // ���������� �� ��������� �������
	for(Spectacle i : sortSpectacles) { cout << i.ToString() << endl; }// ����� ���������������� �������
}
/* ���� ������� */
void kassirMenu() // ������� �������
{
	cout << "������� ����� �������." << endl
		<< "��� ������ ������� 1, ��� ����������� 0" << endl;
	int exit;
	cin >> exit;
	if(exit) { welcome();return; } // ���� ������ ������� �����, ��������� ������� � ������������ �����
	cout << "�������� ��������� ��� �������:" << endl;	
	SchedulePrint(); // ������� ������ ����������
	cout << "�������������� �����." << endl
		<< "������� ���� ���������� ���������:" << endl;
	string date = readDate(); // ������ � ������� ����
	string time = readTime(); // � �����
	for(int i = 0; i < schedule.size(); i++) // ��������� �� �����
	{
		if(0 == schedule.at(i).getDate().compare(date) && 0 == schedule.at(i).getTime().compare(time)) // ���� ������ ��������� ������ ��������� �� �����
		{
			bool prodano = false;
			while(!prodano) // ���� �������� ����� ��������
			{
				cout << "����� ������ ���� �� ������ �������: ������, ����� ����, ������ ����, ������ (1-4):";
				int choice;
				cin >> choice;
				choice--; // � �������� ���������� � 1, � � ��� � 0.
				cout << "������� ��� � �����:";
				int row, place;
				cin >> row >> place;
				bool svobodno = true;
				for(Ticket j : schedule.at(i).getTicket()) // ��������� ��� ������ �� ��� �����������
				{
					if(j.getRow() == row && j.getPlace() == place && j.getType() == choice) // ���� �����, ��� ��� �������
					{
						cout << "����� ��� �������, �������� ������";
						svobodno = false; // �������� ��� �������
						break;			 // � ������ �� ����		
					}
				}
				if(svobodno) // ���� �� ��� �������� (���������� ���� ������ �� �����)
				{
					schedule.at(i).getTicket().push_back(*new Ticket(choice, row, place)); // ��������� ����� ��� ���������
					prodano = true; // � ��������, ��� ��� ����� ��
				}
			}
			break; // ��� ������ ������� ����� ��� �� ������ ������ ��������� � �� �� �����.
		}
	}
	save(); // ��������� ����� � ����
	kassirMenu(); // �������� ������� ������ �����
}

// ������� ���� ��� ��������������
menus adminMenuSelector[6] =
										   {
												ActorPrint,
												SpectaclePrint,
												SchedulePrint,
												AddEntityMenu,
												EditEntityMenu,
											    DeleteEntityMenu
										   };
/* ���� �������������� � ��� �������� */
void adminMenu() // ���� ��������������
{
	cout << "�� ����� � ������� ��������������." << endl;
	cout << "�������� ����� ����: " << endl
		<< "1. �������� ������ ������ ������ ������" << endl
		<< "2. �������� ������ ���������� ������ ������" << endl
		<< "3. �������� ����� ������ ������" << endl
		<< "4. �������� ������������/�����/���������/�����" << endl
		<< "5. ��������������" << endl
		<< "6. ��������" << endl
		<< "7. ��������" << endl;
	int choice;
	cin >> choice;
	if(choice == 7) {main(); return;} // ���� ����� ������ ��������� �����, �������� ������� ������� � ��������� ���
	(*adminMenuSelector[choice - 1])(); // ����� ����������� ������ ����
	system("pause.exe"); // ���������������� ����������, ����� ����� ���� ������ ���������� ����������
	system("cls"); // ������ �����
	adminMenu(); // �������� ���� �����
}
// ������� ���� ��� ���������� ����-�� � ����
menus addMenuSelector[5] =
										   {
												AddUser,
												AddActor,
												AddSpectacle,
												AddSchedule,
												adminMenu
										   };
void AddEntityMenu() // ���� ���������� �������� � ����
{
	cout << "��������, ��� ������ �� ��������:" << endl
		<< "1. ������������" << endl
		<< "2. �����" << endl
		<< "3. ���������" << endl
		<< "4. �����" << endl
		<< "5. ��������" << endl;
	int choice;
	cin >> choice;
	(*addMenuSelector[choice - 1])(); // ����� ����������� ������ ����
	if(choice != 5) // ���� ������ �� �������, � ���-�� ������
	{
		system("cls"); // ����� �������
		save(); // ��������� ����������� � ����
		AddEntityMenu(); // �������� ��� ���� �����
	}
}
void AddUser() { // ���������� ������������
	string rUser, rPassword;
	cout << "���������� ������������." << endl
		<< "������� ��� ������������" <<endl;
	cin >> rUser;
	cout << "������� ������:" << endl;
	cin >> rPassword;
	cout << "��������� �������������� ��� �������, (�/�)";
	char choice;
	cin >> choice;
	string status = choice == '�' ? "kassir" : "admin"; // �������� ������ ������������ � ������� �������
	ofstream usersOUT("users.txt",ios::app); // ��������� ���� ������������� �� ������ � �����������
	usersOUT << rUser << " " << rPassword << " " << status <<endl; // ���������� ������������ � ����
	usersOUT.close(); // ��������� ���� �������������
	
}
void AddActor() { // ���������� �����
	cout << "���������� �����" << endl
	<< "������� ���:";
	string FIO;
	cin.ignore(10, '\n'); // ����� ����� ���� ��������� ������ ���������
	getline(cin, FIO); 
	cout << "������� ���� ��������:" << endl;
	actors.push_back(*new Actor(FIO, readDate())); // ��������� ����� ����� � ���������� ������
	AddEntityMenu(); // ������������ �����
}
void AddSpectacle() { // ���������� ���������
	string title,author,director;
	cout << "���������� ���������" << endl
		<< "������� ��������:";
	cin.ignore(10, '\n'); // ����� ����� ���� ��������� ������ ���������
	getline(cin, title);
	cout << "������� ������:";
	getline(cin, author);
	cout << "������� ������������:";
	getline(cin, director);
	Spectacle spectacle = *new Spectacle(author, title, director); // ������� ����� ��������� �� ������� ������
	while(1) // ���� ��������� ����
	{
		cout << "�������� ����? (y/n):"; 
		char choice;
		cin >> choice;
		if('n' == choice) break; // ���� ���� �� ������� ���������, ������� �� �����
		cout << "������� ����:";
		string tempRole;
		getline(cin, tempRole);		
		ActorPrint(); // ������ ������ ������
		cout << "�������� �����(��� ���������):" << endl;
		string tempFIO;
		getline(cin, tempFIO);
		for(int i = 0; i < actors.size(); i++) // ��������� �� ���� ������
		{
			if(actors.at(i).getFIO().compare(tempFIO) == 0) // ���� ����� ������ ������ ���, ������� ��������� �� ���
			{
				roles.push_back(*new Role(spectacle, actors.at(i),tempRole)); // ��������� ����� � ������ �����
				cout << "���� ��������";
				break; // ������� �� ����� �����
			}
		}

	}
	spectacles.push_back(spectacle); // ��������� ��������� ��������� � ����� ���������� ����������
	AddEntityMenu(); // �������� ���� ����
}
void AddSchedule() { // ���������� �����
	cout << "���������� �����" << endl
		<< "�������� ���������(������ ��������):";
	SpectaclePrint(); // ������� ������ ����������
	string tempTitle;
	cin.ignore(10, '\n');// ����� ����� ���� ��������� ������ ���������
	getline(cin, tempTitle);
	Spectacle spectacle = Spectacle::find(tempTitle); // ���� ��������� � ����� ������ � ������� ��������� �������
	cout << "������� ���� ����������:" << endl;
	string date = readDate(); // ������ ���� � �������
	string time = readTime(); // ������ ����� � �������
	schedule.push_back(*new Schedule(spectacle, date, time)); // ��������� ����� �����
}
// ������� ���� �������������
menus editMenuSelector[5] =
										   {
												EditActor,
												EditSpectacle,
												EditSchedule,
												adminMenu
										   };
void EditEntityMenu() { // ���� ��������������
	cout << "��������, ��� ������ �� ���������������:" << endl	
		<< "1. �����" << endl
		<< "2. ���������" << endl
		<< "3. �����" << endl
		<< "4. ��������" << endl;
	int choice;
	cin >> choice;
	(*editMenuSelector[choice - 1])(); // ����� ����������� ������ ����
	if(choice != 4) // ���� ������������ ����� �����
	{
		system("cls"); // ������ �������
		save(); // ��������� � ����
		EditEntityMenu(); // �������� ��� �� ����
	}
}
void EditActor() { // ����������� �����
	ActorPrint(); // ������� ������ ������
	cout << "�������� �����(��� ���������):" << endl;	
	string tempFIO;
	cin.ignore(10, '\n'); // ����� ��������� ��������� ��� ������
	getline(cin, tempFIO);
	for(int i = 0; i < actors.size(); i++) // ��������� �� ������
	{
		if(actors.at(i).getFIO().compare(tempFIO) == 0) // ���� ������� ���� �� ��� ����� ��������� � �������
		{
			cout << "�������������� ����� " << tempFIO << endl
				<< "������������� ��� ��� ���� ��������?(n/d):";
			char choice;
			cin >> choice;
			if('n' == choice) // ���� ����� �������� ���
			{
				cout << "������� ���:";
				string FIO;
				cin.ignore(10, '\n');
				getline(cin, FIO);
				actors.at(i).setFIO(FIO); // ������ �������������� ��� � �������� �����
			}
			else if ('d' == choice) actors.at(i).setBirthday(readDate()); // ���� ����� ������� ���� ��������, �������������� �
			else cout << "���� �������."; // ����� �������
			return;
		}
	}
}
void EditSpectacle() { // �������������� ���������
	SpectaclePrint(); // ������ ������ ����������
	cout << "�������������� ���������." << endl
		<< "������� �������� ��������� ���������: ";
	string tempTitle;
	cin.ignore(10, '\n'); // ����� ��������� ��������� ������
	getline(cin, tempTitle);

	cout << "�������������� ��������� " << tempTitle << endl
		<< "������������� a�����, �������� ��� ������������?(a/n/d):";
	char choice;
	cin >> choice;
	if('n' == choice) // ���� ����� ������� ��������
	{
		cout << "������� ��������:";
		string title;
		cin.ignore(10, '\n'); // ����� ��������� ��������� ������
		getline(cin, title);
		for(int i = 0; i < spectacles.size(); i++)
			if(spectacles.at(i).getTitle().compare(tempTitle) == 0) spectacles.at(i).setIitle(title); // ������ ������ � ����� ��������� ��������
	}
	else
	if('a' == choice){// ���� ����� ������� ������
		cout << "������� ��� ������:";
		string author;
		cin.ignore(10, '\n'); // ����� ��������� ��������� ������
		getline(cin, author);
		for(int i = 0; i < spectacles.size(); i++)
			if(spectacles.at(i).getAuthor().compare(tempTitle) == 0) spectacles.at(i).setAuthor(author); // ������ ������ � ����� ��������� ������		
	}
	else
	if('d' == choice)// ���� ����� ������� ������������
	{
		cout << "������� ��� ������������:";
		string director;
		cin.ignore(10, '\n'); // ����� ��������� ��������� ������
		getline(cin, director);
		for(int i = 0; i < spectacles.size(); i++)	
			if(spectacles.at(i).getDirector().compare(tempTitle) == 0) spectacles.at(i).setDirector(director);// ������ ������ � ����� ��������� ������������
	}
}
void EditSchedule() { // �������������� �����
	SchedulePrint(); // ������� �����
	cout << "�������������� �����." << endl
		<< "������� ���� ���������� ���������:" <<endl;
	string date = readDate(); // ������ ���� � �������
	string time = readTime(); // ������ ����� � �������
	for(int i = 0; i < schedule.size(); i++) // ����������� �� �����
	{
		if(0 == schedule.at(i).getDate().compare(date) && 0 == schedule.at(i).getTime().compare(time)) // ���� ������� �����, ����������� �� ������� � ����
		{
			cout << "�������� ���� ��� �����? (d/t):" << endl;
			char choice;
			cin >> choice;
			if('d' == choice) schedule.at(i).setDate(readDate()); // ���� ����� �������� ���� - ������ �
			else if('t' == choice ) schedule.at(i).setTime(readTime()); // ���� ����� �������� ����� - ������ ���
			else cout << "���� �������. ��������� � ���� ����."; // ���� ���� �������, ����� �� ���� � ��������� �������.
			return;
		}
	}
}
menus DeleteMenuSelector[4] =
{
	DelActor,
	DelSpectacle,
	DelSchedule,
	adminMenu
};
void DeleteEntityMenu() {
	cout << "��������, ��� ������ �� �������:" << endl
		<< "1. �����" << endl
		<< "2. ���������" << endl
		<< "3. �����" << endl
		<< "4. ��������" << endl;
	int choice;
	cin >> choice;
	(*DeleteMenuSelector[choice - 1])(); // ����� ����������� ������ ����
	if(choice != 4) // ���� ������ �� �������, � ���-�� ������
	{
		system("cls"); // ����� �������
		save(); // ��������� ����������� � ����
		DeleteEntityMenu(); // �������� ��� ���� �����
	}
}
void DelActor(){ // �������� �����
	ActorPrint(); // ������� ������ ������
	cout << "�������� �����(��� ���������):" << endl;
	string tempFIO;
	cin.ignore(10, '\n'); // ����� ��������� ��������� ��� ������
	getline(cin, tempFIO);
	for(int i = 0; i < actors.size(); i++) // ��������� �� ������
	{
		if(actors.at(i).getFIO().compare(tempFIO) == 0) // ���� ������� ���� �� ��� ����� ��������� � �������
		{
			actors.erase(actors.begin() + i); // �������� �� ������� ������ �� ��������� �� ���� ������� 
			cout << "���� �����" << endl;
			return;
		}
	}
}
void DelSpectacle(){ // �������� ���������
	SpectaclePrint(); // ������ ������ ����������
	cout << "�������� ���������." << endl
		<< "������� �������� ��������� ���������: ";
	string tempTitle;
	cin.ignore(10, '\n'); // ����� ��������� ��������� ������
	getline(cin, tempTitle);
	for(int i = 0; i < spectacles.size(); i++)
		if(spectacles.at(i).getTitle().compare(tempTitle) == 0) // ���� ������ ���������, ����������� �� ��������
		{
			spectacles.erase(spectacles.begin() + i); // �������� �� ������� ������ �� ��������� �� ���� ������� 
			cout << "��������� �����" << endl;
			return;
		}
	
}
void DelSchedule() { // �������� �����
	SchedulePrint(); // ������� �����
	cout << "�������� �����." << endl
		<< "������� ���� ���������� ���������:" << endl;
	string date = readDate(); // ������ ���� � �������
	string time = readTime(); // ������ ����� � �������
	for(int i = 0; i < schedule.size(); i++) // ����������� �� �����
	{
		if(0 == schedule.at(i).getDate().compare(date) && 0 == schedule.at(i).getTime().compare(time)) // ���� ������� �����, ����������� �� ������� � ����
		{
			schedule.erase(schedule.begin() + i);// �������� �� ������� ������ �� ��������� �� ���� ������� 
			cout << "����� �������";
			return;
		}
	}
}